#!/bin/sh

#
# 2017-2018 © BIM & Scan® Ltd.
# See 'LICENCE.md' in the project root for more information.
#

set -e
conan remote add "bimandscan-private" $ARTIFACTORY_REMOTE_BIMANDSCAN_PUBLIC
conan remote add "bintray-cache" $ARTIFACTORY_REMOTE_BIMANDSCAN_BINTRAY_CACHE --insert 0
conan user -p $ARTIFACTORY_REMOTE_BIMANDSCAN_KEY -r "bimandscan-private" $ARTIFACTORY_REMOTE_BIMANDSCAN_USER
