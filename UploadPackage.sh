#!/bin/sh

#
# 2017-2018 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "libe57/1.1.332@bimandscan/stable"
