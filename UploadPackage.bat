@ECHO OFF

REM
REM 2017-2018 © BIM & Scan® Ltd.
REM See 'LICENCE.md' in the project root for more information.
REM

CALL conan upload -c -r "bimandscan-public" --all "libe57/1.1.332@bimandscan/stable"

@ECHO ON
