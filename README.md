# BIM & Scan® Third-Party Library (libE57)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [libE57](http://www.libe57.org/), software tools for managing E57 files.

Supports version 1.1.332 (stable/modified for easier use in other projects).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) and [BIM & Scan® (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repositories for third-party dependencies.
