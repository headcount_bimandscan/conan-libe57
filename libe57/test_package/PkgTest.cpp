/*
 * 2017-2018 © BIM & Scan® Ltd.
 * See 'LICENCE.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>
#include <vector>
#include <numeric>

#include <e57/Foundation.h>
#include <e57/Simple.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "libE57 package test (compilation, linking, and execution).\n";

    e57::Reader* reader;
    e57::Writer* writer;

    int version_major = 0,
        version_minor = 0;

    std::string library_id;

    e57::E57Utilities().getVersions(version_major,
                                    version_minor,
                                    library_id);

    std::cout << "Built with ASTM E57 support v" << version_major << "." << version_minor << " (" << library_id << ")\n";

    std::cout << "libE57 package works!" << std::endl;
    return EXIT_SUCCESS;
}
