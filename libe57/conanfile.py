#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2017-2018 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#
from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile

class LibE57(ConanFile):
    name = "libe57"
    version = "1.1.332"
    license = "MIT"
    url = "https://bitbucket.org/headcount_bimandscan/conan-e57refimpl"
    description = "Software tools for managing E57 files."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"

    _homepage = "http://www.libe57.org"
    _src_dir = "libe57_refimpl_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    exports = "../LICENCE.md"
    exports_sources = "CMakeLists.txt", \
                      "libe57_src/*"

    requires = "boost_thread/1.69.0@bincrafters/stable", \
               "boost_system/1.69.0@bincrafters/stable", \
               "boost_uuid/1.69.0@bincrafters/stable", \
               "boost_date_time/1.69.0@bincrafters/stable", \
               "boost_filesystem/1.69.0@bincrafters/stable", \
               "boost_crc/1.69.0@bincrafters/stable", \
               "icu/62.1@bincrafters/stable", \
               "xerces_c/3.2.2@bimandscan/stable", \
               "boost_program_options/1.69.0@bincrafters/stable", \
               "cmake_findboost_modular/1.69.0@bincrafters/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def _configure_cmake(self):
        cmake = CMake(self,
                      set_cmake_flags=True)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC
            cmake.definitions["CMAKE_CXX_FLAGS"] += " -DLINUX"
            cmake.definitions["CMAKE_C_FLAGS"] += " -DLINUX"
        elif self.settings.compiler == "Visual Studio":
            cmake.definitions["CMAKE_CXX_FLAGS"] += " /DWIN32"
            cmake.definitions["CMAKE_C_FLAGS"] += " /DWIN32"
        else:
            cmake.definitions["CMAKE_CXX_FLAGS"] += " -DWIN32"
            cmake.definitions["CMAKE_C_FLAGS"] += " -DWIN32"

        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.configure(source_folder = "libe57_src")
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

        self.copy("LICENSE.txt",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os == "Windows":
            self.cpp_info.cflags.append("/DWIN32")
            self.cpp_info.cppflags.append("/DWIN32")
        else:
            self.cpp_info.cflags.append("-DLINUX")
            self.cpp_info.cppflags.append("-DLINUX")
