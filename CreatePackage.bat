@ECHO OFF

REM
REM 2017-2018 © BIM & Scan® Ltd.
REM See 'LICENCE.md' in the project root for more information.
REM

CALL conan create --build=missing %* "libe57" bimandscan/stable

@ECHO ON
