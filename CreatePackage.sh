#!/bin/sh

#
# 2017-2018 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#

set -e
conan create --build=missing "$@" "libe57" bimandscan/stable
